Eazy.resultsViews.push({
    name: "Test Calendar",
    key: "test_calendar",
    view: "TestCalendarView",
    icon: "fa fa-map-marker"
});


TestCalendarView = Eazy.ChartView.extend({

    loadAPI: function() {
        if (window.google) {
          return true;
        } else if (!this.constructor.apiLoading) {
          this.constructor.apiLoading = true;
          $("head").append("<script src=\"https://www.gstatic.com/charts/loader.js\" type=\"text/javascript\">");
          return false;
        } else {
          return false;
        }
      },

    render: function() {

    function drawChart() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn({ type: 'date', id: 'Date' });
        dataTable.addColumn({ type: 'number', id: 'Data' });


        
        // V This does not work V
        //var queryResult = this.model.queryResult;
        //this.categories = queryResult.chartCategories("row");
        //this.series = queryResult.chartSeries("column");

            // set up the series/category that comes from eazyBI to use later
            // var category = this.categories[0],
            //     dateSeries = {
            //         name: this.series[0].name,
            //         //data: new Date([this.series[0].data[ci]])
            //     },
            //     dataSeries = {
            //         name: this.series[1].name,
            //         data: [this.series[1].data[ci]]
            //     };

        //dataTable.addRows([dateSeries.data, dataSeries.data]);

        
        // V This works statically! V
        // dataTable.addRows([
        //     [ new Date(2012, 3, 13), 37032 ],
        //     [ new Date(2012, 3, 14), 38024 ],
        //     [ new Date(2012, 3, 15), 38024 ],
        //     [ new Date(2012, 3, 16), 38108 ],
        //     [ new Date(2012, 3, 17), 38229 ],
        //     [ new Date(2013, 9, 4), 38177 ],
        //     [ new Date(2013, 9, 5), 38705 ],
        //     [ new Date(2013, 9, 12), 38210 ],
        //     [ new Date(2013, 9, 13), 38029 ],
        //     [ new Date(2013, 9, 19), 38823 ],
        //     [ new Date(2013, 9, 23), 38345 ],
        //     [ new Date(2013, 9, 24), 38436 ],
        //     [ new Date(2013, 9, 30), 38447 ]
        //     ])


            
            var chart = new google.visualization.Calendar(document.getElementById('calendar_basic'));
            var options = {
                title: "Test Dates",
                height: 350,
              };
            chart.draw(dataTable, options);
        };

    var $div = $("<div id=\"calendar_basic\"><div/>")
        .css({
        display: "inline-block",
        width: "1000px",
        height: "300px",
        overflowX: "hidden",
        marginBottom: "10px"
        }).appendTo(this.el);



    if (!this.loadAPI()) {
        // retry after 1 second as Google Maps API will probably be loaded then
        _.delay(_.bind(this.render, this), 1000);
        return;
  }

    google.charts.load("current", {packages:["calendar"]});
    google.charts.setOnLoadCallback(drawChart);


    return this;
    },  
    


})
